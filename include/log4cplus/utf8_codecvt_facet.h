// Copyright (c) 2001 Ronald Garcia, Indiana University (garcia@osl.iu.edu)
// Andrew Lumsdaine, Indiana University (lums@osl.iu.edu). Permission to copy, 
// use, modify, sell and distribute this software is granted provided this
// copyright notice appears in all copies. This software is provided "as is"
// without express or implied warranty, and with no claim as to its suitability
// for any purpose.

#ifndef LOG4CPLUS_UTF8_CODECVT_FACET_HPP
#define LOG4CPLUS_UTF8_CODECVT_FACET_HPP

#include <log4cplus/config.hxx>

#define BOOST_UTF8_BEGIN_NAMESPACE \
	namespace log4cplus { namespace boost {

#define BOOST_UTF8_END_NAMESPACE }}
#define BOOST_UTF8_DECL LOG4CPLUS_EXPORT

#include "utf8_codecvt_facet.hpp"

#undef BOOST_UTF8_BEGIN_NAMESPACE
#undef BOOST_UTF8_END_NAMESPACE
#undef BOOST_UTF8_DECL

#endif // LOG4CPLUS_UTF8_CODECVT_FACET_HPP
