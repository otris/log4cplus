#include <log4cplus/config.hxx>

#define BOOST_UTF8_BEGIN_NAMESPACE \
	namespace log4cplus { namespace boost {
#define BOOST_UTF8_END_NAMESPACE }}
#define BOOST_UTF8_DECL LOG4CPLUS_EXPORT

#include "utf8_codecvt_facet.cpp"

#undef BOOST_UTF8_BEGIN_NAMESPACE
#undef BOOST_UTF8_END_NAMESPACE
#undef BOOST_UTF8_DECL
